My Summary of sketchnotes, that I want to share.

This Repo contains high resolution versions of the sketchnotes. Sometimes more than 15 MB per sketchnote. Keep that in mind, when you access this repo with your mobile.

All content is licensed under CC-BY-NC-SA 4.0, unless otherwise marked, more in the LICENSE file.



Mein Sammlung von Sketchnotes, die ich freigeben will.

Diese Repo enthält die hochaufgelösten Versionen meiner Sketchnotes. Teilweise sind die großer als 15 MB. Das ist besonders dann zu bedenken, wenn das Repo mit dem Handy aufgerufen wird.

Alles ist unter CC-BY-NC-SA 4.0 lizensiert, wenn nicht anders angegeben, mehr dazu in der LICENSE Datei.
